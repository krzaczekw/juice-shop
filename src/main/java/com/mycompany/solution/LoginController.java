/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solution;

import com.mycompany.solution.backend.service.CustomerService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author Sendit
 */
public class LoginController implements Initializable {

    public final CustomerService customerService = new CustomerService();
    @FXML
    private Button loginbutton;
    @FXML
    private TextField logintext;
    @FXML
    private PasswordField passwordtext;
    @FXML
    private ImageView logo;

    @FXML
    void SigningIn(ActionEvent event) throws IOException {
        String login = logintext.getText();
        String password = passwordtext.getText();
        System.out.println(login + password);

        if (login.isBlank() || password.isBlank()) {
            showWarningDialog();
        } else if (customerService.login(login, password)) {
//            showInfoDialog();
            switchToPrimary();
        } else {
            showWarningDialog();
        }

    }

    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }

    @FXML
    public void showInfoDialog(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Login");
        alert.setContentText("You have been logged successfully!");

        alert.showAndWait();
    }

    @FXML
    public void showInfoDialog() throws IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Login");
        alert.setContentText("You have been logged successfully!");

        alert.showAndWait();
    }

    @FXML
    public void showWarningDialog(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Login");
        alert.setContentText("Invalid password or user name, try again");

        alert.showAndWait();
    }

    @FXML
    public void showWarningDialog() throws IOException {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Login");
        alert.setContentText("Invalid password or user name, try again");

        alert.showAndWait();
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        File file = new File("fotos/logo.png");
        Image image = new Image(file.toURI().toString());
        logo.setImage(image);
    }

}
