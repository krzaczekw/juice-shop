package com.mycompany.solution;

import com.mycompany.solution.backend.model.Asset;
import com.mycompany.solution.backend.service.AssetService;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrimaryController implements Initializable {

    AssetService assetService = new AssetService();
    @FXML
    private Button buttonGL;
    @FXML
    private Button buttonMSM;
    @FXML
    private Button buttonS;
    @FXML
    private ImageView logo2;
    @FXML
    private TableColumn<Asset, Integer> columnID;
    @FXML
    private TableColumn<Asset, String> columnAdd;
    @FXML
    private TableColumn<Asset, String> columnAsset;
    @FXML
    private TableColumn<Asset, Double> columnCurrentStock;
    @FXML
    private TableColumn<Asset, Double> columnRemove;
    @FXML
    private TableView<Asset> tableStock;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        File file = new File("fotos/logo.png");
        Image image = new Image(file.toURI().toString());
        logo2.setImage(image);
        ObservableList<Asset> assets = FXCollections.observableArrayList(assetService.getAll());

        /*columnID.setCellValueFactory(new PropertyValueFactory<Asset, Integer>("AssetID"));
        columnAsset.setCellValueFactory(new PropertyValueFactory<Asset, String>("Asset"));
        columnCurrentStock.setCellValueFactory(new PropertyValueFactory<Asset, Double>("Stock"));
        columnAdd.setCellValueFactory(new PropertyValueFactory<Asset, Double>("Add"));
        columnRemove.setCellValueFactory(new PropertyValueFactory<Asset, Double>("Remove"));*/

        columnID.setCellValueFactory(new PropertyValueFactory<Asset, Integer>("id"));
        columnAsset.setCellValueFactory(new PropertyValueFactory<Asset, String>("name"));
        columnCurrentStock.setCellValueFactory(new PropertyValueFactory<Asset, Double>("stock"));

        tableStock.setItems(assets);
        tableStock.setEditable(true);

        columnAdd.setCellFactory(tc -> {

            TableCell<Asset, String> cell = new TableCell<>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(empty ? null : item);
                }
            };

            cell.setOnMouseClicked(e -> {
                if (e.getClickCount() == 2 && !cell.isEmpty()) {
                    showEditingWindow(tableStock.getScene().getWindow(), cell.getItem(), newValue -> {

                        Asset asset = tableStock.getItems().get(cell.getIndex());
                        double temp = Double.parseDouble(newValue.toString()); //temp to to co wpisane do okienka
                        asset.setStock(temp);
                        System.out.print(asset.getId());
                        assetService.update(asset);
                        try {
                            App.setRoot("primary");
                        } catch (IOException ex) {
                            Logger.getLogger(PrimaryController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    });//dotąd sie wykonuje po kliknięciu ok
                }
            });
            return cell;
        });

    }

    private void showEditingWindow(Window owner, String currentValue, Consumer<String> commitHandler) {
        Stage stage = new Stage();
        stage.initOwner(owner);
        stage.initModality(Modality.APPLICATION_MODAL);

        TextArea textArea = new TextArea(currentValue);

        Button okButton = new Button("OK");
        okButton.setDefaultButton(true);
        okButton.setOnAction(e -> {
            commitHandler.accept(textArea.getText());
            stage.hide();
        });

        Button cancelButton = new Button("Cancel");
        cancelButton.setCancelButton(true);
        cancelButton.setOnAction(e -> stage.hide());

        HBox buttons = new HBox(5, okButton, cancelButton);
        buttons.setAlignment(Pos.CENTER);
        buttons.setPadding(new Insets(5));

        BorderPane root = new BorderPane(textArea, null, null, buttons, null);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    private void switchToSecondary() throws IOException {
        App.setRoot("secondary");
    }

    @FXML
    public void showInfoDialog() throws IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText("Login");
        alert.setContentText("You have been logged successfully!");

        alert.showAndWait();
    }

    public static class Item {
        private final StringProperty description = new SimpleStringProperty();
        private final StringProperty name = new SimpleStringProperty();

        public Item(String name, String description) {
            setName(name);
            setDescription(description);
        }

        public final StringProperty descriptionProperty() {
            return this.description;
        }


        public final String getDescription() {
            return this.descriptionProperty().get();
        }


        public final void setDescription(final String description) {
            this.descriptionProperty().set(description);
        }


        public final StringProperty nameProperty() {
            return this.name;
        }


        public final String getName() {
            return this.nameProperty().get();
        }


        public final void setName(final String name) {
            this.nameProperty().set(name);
        }

    }
}
