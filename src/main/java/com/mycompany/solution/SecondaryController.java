package com.mycompany.solution;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;

public class SecondaryController {

    @FXML
    private Button buttonGL;

    @FXML
    private Button buttonMSM;

    @FXML
    private Button buttonS;

    @FXML
    private Button buttonSaveOrder;

    @FXML
    private TableColumn<?, ?> columnComposure;

    @FXML
    private TableColumn<?, ?> columnID;

    @FXML
    private TableColumn<?, ?> columnJuice;

    @FXML
    private TableColumn<?, ?> columnOrder;

    @FXML
    private ImageView logoImage;

    @FXML
    private TableView<?> tableOrdering;
    
    @FXML
    private void switchToPrimary() throws IOException {
        App.setRoot("primary");
    }
}
