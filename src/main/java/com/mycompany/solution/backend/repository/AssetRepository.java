package com.mycompany.solution.backend.repository;

import com.mycompany.solution.backend.model.Asset;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AssetRepository {
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:db.sqlite3";

    private PreparedStatement createStatement(String statement) throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL);
        return conn.prepareStatement(statement);
    }

    public Asset get(int id) {
        try (PreparedStatement statement = createStatement("SELECT * FROM Asset WHERE id=?;")) {
            statement.setInt(1, id);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next()) {
                return getAsset(resultSet);
            } else {
                return null;
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when fetching assets: " + e);
            return null;
        }
    }

    public List<Asset> getAll() {
        try (PreparedStatement statement = createStatement("SELECT * FROM Asset;")) {
            statement.execute();
            return getAssets(statement.getResultSet());
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when fetching assets: " + e);
            return Collections.emptyList();
        }
    }

    public List<Asset> getAllUnderThreshold() {
        try (PreparedStatement statement = createStatement("SELECT * FROM Asset WHERE Stock<Threshold;")) {
            statement.execute();
            return getAssets(statement.getResultSet());
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when fetching assets: " + e);
            return Collections.emptyList();
        }
    }

    private List<Asset> getAssets(ResultSet result) throws SQLException {
        List<Asset> assets = new ArrayList<>();
        while (result.next()) {
            assets.add(getAsset(result));
        }

        return assets;
    }

    private Asset getAsset(ResultSet result) throws SQLException {
        return new Asset(result.getInt("id"),
                result.getString("Asset"),
                result.getDouble("Stock"),
                result.getDouble("Threshold"));
    }

    public void save(Asset asset) {
        try (PreparedStatement statement = createStatement("UPDATE Asset SET Stock = ?, Asset = ?, Threshold = ? WHERE id = ?;")) {
            statement.setDouble(1, asset.getStock());
            statement.setString(2, asset.getName());
            statement.setDouble(3, asset.getThreshold());
            statement.setInt(4, asset.getId());
            statement.execute();
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when saving asset: " + e);
        }
    }

    public void create(Asset asset) {
        try (PreparedStatement statement = createStatement("INSERT INTO Asset (Asset, Stock, Threshold) VALUES(?, ?, ?);")) {
            statement.setString(1, asset.getName());
            statement.setDouble(2, asset.getStock());
            statement.setDouble(3, asset.getThreshold());
            statement.execute();
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when saving asset: " + e);
        }
    }

}
