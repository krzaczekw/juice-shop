package com.mycompany.solution.backend.repository;

import com.mycompany.solution.backend.model.Asset;
import com.mycompany.solution.backend.model.Ingredient;
import com.mycompany.solution.backend.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductRepository {
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:db.sqlite3";

    private PreparedStatement createStatement(String statement) throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL);
        return conn.prepareStatement(statement);
    }

    public List<Product> getProducts() {
        try (PreparedStatement statement = createStatement("SELECT * FROM Product p;")) {
            statement.execute();
            ResultSet result = statement.getResultSet();
            List<Product> products = new ArrayList<>();
            while (result.next()) {
                int productId = result.getInt("id");
                products.add(new Product(productId, result.getString("name"), getProductIngredients(productId)));
            }
            return products;
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when fetching list of products: " + e);
            return Collections.emptyList();
        }
    }

    public Product getProduct(int id) {
        try (PreparedStatement statement = createStatement("SELECT * FROM Product p WHERE id=?;")) {
            statement.setInt(1, id);
            statement.execute();
            ResultSet result = statement.getResultSet();
            if (result.next()) {
                return new Product(result.getInt("id"), result.getString("name"), getProductIngredients(id));
            } else {
                return null;
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when fetching product: " + e);
            return null;
        }
    }

    private ArrayList<Ingredient> getProductIngredients(int productId) throws SQLException, ClassNotFoundException {
        try (PreparedStatement statement = createStatement("SELECT *, a.id assetId FROM Ingredient i JOIN Asset a on i.assetId=a.id WHERE i.productId=?;")) {
            statement.setInt(1, productId);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            ArrayList<Ingredient> ingredients = new ArrayList<>();
            while (resultSet.next()) {
                ingredients.add(new Ingredient(new Asset(resultSet.getInt("assetId"), resultSet.getString("Asset"), resultSet.getDouble("Stock"), resultSet.getDouble("Threshold")), resultSet.getDouble("quantity")));
            }
            return ingredients;
        }
    }

    public void create(Product product) {
        try (PreparedStatement statement = createStatement("INSERT INTO Product (name) VALUES(?);")) {
            statement.setString(1, product.getName());
            statement.execute();
            ResultSet resultSet = statement.getGeneratedKeys();
            resultSet.next();
            int productId = resultSet.getInt(1);
            product.getIngredients().forEach(ingredient -> saveIngredients(productId, ingredient));
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when saving product: " + e);
        }
    }

    private void saveIngredients(int productId, Ingredient ingredient) {
        try (PreparedStatement ingredientStatement = createStatement("INSERT INTO Ingredient (productId, assetId, quantity) VALUES(?, ?, ?);")) {
            ingredientStatement.setInt(1, productId);
            ingredientStatement.setInt(2, ingredient.getAsset().getId());
            ingredientStatement.setDouble(3, ingredient.getQuantity());
            ingredientStatement.execute();
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when saving ingredient: " + e);
        }
    }

    public void delete(int productId) {
        try (PreparedStatement statement = createStatement("DELETE FROM Product WHERE id=?;")) {
            statement.setInt(1, productId);
            statement.execute();
            deleteIngredients(productId);
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when saving ingredient: " + e);
        }
    }

    private void deleteIngredients(int productId) throws SQLException, ClassNotFoundException {
        try (PreparedStatement ingredientStatement = createStatement("DELETE FROM Ingredient WHERE productId=?;")) {
            ingredientStatement.setInt(1, productId);
            ingredientStatement.execute();
        }
    }

    public void update(Product product) {
        try (PreparedStatement statement = createStatement("UPDATE Product SET name=? WHERE id=?;")) {
            statement.setString(1, product.getName());
            statement.setInt(2, product.getId());
            statement.execute();
            deleteIngredients(product.getId());
            product.getIngredients().forEach(ingredient -> saveIngredients(product.getId(), ingredient));
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when saving product: " + e);
        }
    }
}
