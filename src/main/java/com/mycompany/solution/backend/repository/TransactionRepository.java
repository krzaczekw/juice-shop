package com.mycompany.solution.backend.repository;

import com.mycompany.solution.backend.model.Transaction;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TransactionRepository {
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:db.sqlite3";

    private PreparedStatement createStatement(String statement) throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL);
        return conn.prepareStatement(statement);
    }

    public void create(Transaction transaction) {
        try (PreparedStatement statement = createStatement("INSERT INTO Trans (product_name, quantity, date) VALUES(?, ?, ?);")) {
            statement.setString(1, transaction.getProduct());
            statement.setInt(2, transaction.getQuantity());
            statement.setDate(3, Date.valueOf(transaction.getDate()));
            statement.execute();
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when saving transaction: " + e);
        }
    }

    public List<Transaction> getTransactions(LocalDate from, LocalDate to) {
        try (PreparedStatement statement = createStatement("SELECT * FROM Trans t WHERE date BETWEEN ? AND ?;")) {
            statement.setDate(1, Date.valueOf(from.toString()));
            statement.setDate(2, Date.valueOf(to.toString()));
            statement.execute();
            ResultSet result = statement.getResultSet();
            List<Transaction> transactions = new ArrayList<>();
            while (result.next()) {
                transactions.add(new Transaction(result.getString("product_name"), result.getInt("quantity"), result.getDate("date").toLocalDate()));
            }
            return transactions;
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when fetching list of transactions: " + e);
            return Collections.emptyList();
        }
    }
}
