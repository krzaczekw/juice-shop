package com.mycompany.solution.backend.repository;

import com.mycompany.solution.backend.model.Customer;

import java.sql.*;
import java.util.Optional;

public class CustomerRepository {
    public static final String DRIVER = "org.sqlite.JDBC";
    public static final String DB_URL = "jdbc:sqlite:db.sqlite3";

    private PreparedStatement createStatement(String statement) throws ClassNotFoundException, SQLException {
        Class.forName(DRIVER);
        Connection conn = DriverManager.getConnection(DB_URL);
        return conn.prepareStatement(statement);
    }

    public Optional<Customer> getCustomer(String login) {
        try (PreparedStatement statement = createStatement("SELECT * FROM Customer WHERE Login=?;")) {
            statement.setString(1, login);
            statement.execute();
            ResultSet result = statement.getResultSet();
            if (!result.next()) {
                return Optional.empty();
            } else {
                return Optional.of(new Customer(result.getString("Login"), result.getString("Password")));
            }
        } catch (SQLException | ClassNotFoundException e) {
            System.out.println("Exception when fetching customer: " + e);
            return Optional.empty();
        }
    }
}
