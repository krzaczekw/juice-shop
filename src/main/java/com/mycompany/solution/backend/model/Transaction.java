package com.mycompany.solution.backend.model;

import java.time.LocalDate;

public class Transaction {
    private String productName;
    private int quantity;
    private LocalDate date;

    public Transaction(String productName, int quantity, LocalDate date) {
        this.productName = productName;
        this.quantity = quantity;
        this.date = date;
    }

    public String getProduct() {
        return productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public LocalDate getDate() {
        return date;
    }
}
