package com.mycompany.solution.backend.model;

import java.util.List;

public class Product {
    private int id;
    private String name;
    private List<Ingredient> ingredients;

    public Product(int id, String name, List<Ingredient> ingredients) {
        this(name, ingredients);
        this.id = id;
    }

    public Product(String name, List<Ingredient> ingredients) {
        this.name = name;
        this.ingredients = ingredients;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }
}
