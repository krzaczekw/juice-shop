package com.mycompany.solution.backend.model;

public class Ingredient {
    private Asset asset;
    private double quantity;

    public Ingredient(Asset asset, double quantity) {
        this.asset = asset;
        this.quantity = quantity;
    }

    public Asset getAsset() {
        return asset;
    }

    public double getQuantity() {
        return quantity;
    }
}
