package com.mycompany.solution.backend.model;

public class Asset {
    private int id;
    private String name;
    private double stock;
    private double threshold;

    public Asset(int id, String name, double stock, double threshold) {
        this(name, stock, threshold);
        this.id = id;
    }

    public Asset(String name, double stock, double threshold) {
        this.threshold = threshold;
        this.name = name;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public double getThreshold() {
        return threshold;
    }

    public void addStock(double stock) {
        this.stock += stock;
    }

    public void removeStock(double stock) {
        this.stock -= stock;
        if (this.stock < 0) {
            this.stock = 0;
        }
    }

}
