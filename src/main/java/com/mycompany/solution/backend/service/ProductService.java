package com.mycompany.solution.backend.service;

import com.mycompany.solution.backend.model.Asset;
import com.mycompany.solution.backend.model.Product;
import com.mycompany.solution.backend.model.Transaction;
import com.mycompany.solution.backend.repository.AssetRepository;
import com.mycompany.solution.backend.repository.ProductRepository;
import com.mycompany.solution.backend.repository.TransactionRepository;

import java.time.LocalDate;
import java.util.List;

public class ProductService {

    private final ProductRepository repository = new ProductRepository();
    private final AssetRepository assetRepository = new AssetRepository();
    private final TransactionRepository transactionRepository = new TransactionRepository();

    public static void main(String[] args) {
        new ProductService().sellProduct(1, 35);
    }

    public List<Product> getProducts() {
        return repository.getProducts();
    }

    public void sellProduct(int productId, int quantity) {
        Product product = repository.getProduct(productId);
        product.getIngredients().forEach(
                ingredient -> {
                    Asset asset = assetRepository.get(ingredient.getAsset().getId());
                    asset.removeStock(ingredient.getQuantity() * quantity);
                    assetRepository.save(asset);
                }
        );
        transactionRepository.create(new Transaction(product.getName(), quantity, LocalDate.now()));
    }

    public void addProduct(Product product) {
        repository.create(product);
    }

    public void deleteProduct(int productId) {
        repository.delete(productId);
    }

    public void saveProduct(Product product) {
        repository.update(product);
    }
}
