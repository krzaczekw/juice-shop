package com.mycompany.solution.backend.service;

import com.mycompany.solution.backend.model.Customer;
import com.mycompany.solution.backend.repository.CustomerRepository;

import java.util.Optional;

public class CustomerService {

    private final CustomerRepository repository = new CustomerRepository();

    public boolean login(String login, String password) {
        Optional<Customer> optionalCustomer = repository.getCustomer(login);
        if (optionalCustomer.isEmpty()) {
            System.out.printf("Customer with login %s not found%n", login);
            return false;
        } else {
            Customer customer = optionalCustomer.get();
            if (customer.getPassword().equals(password)) {
                System.out.println("User login successful");
                return true;
            } else {
                System.out.printf("Incorrect password for Customer with login %s", login);
                return false;
            }
        }
    }
}
