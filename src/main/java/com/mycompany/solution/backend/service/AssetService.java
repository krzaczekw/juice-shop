package com.mycompany.solution.backend.service;

import com.mycompany.solution.backend.model.Asset;
import com.mycompany.solution.backend.repository.AssetRepository;

import java.util.List;

public class AssetService {

    private final AssetRepository repository = new AssetRepository();

    public List<Asset> getAll() {
        return repository.getAll();
    }

    public List<Asset> getAssetsUnderThreshold() {
        return repository.getAllUnderThreshold();
    }

    public void update(Asset asset) {
        repository.save(asset);
    }

    public void create(Asset asset) {
        repository.create(asset);
    }

}
