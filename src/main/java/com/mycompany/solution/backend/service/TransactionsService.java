package com.mycompany.solution.backend.service;

import com.mycompany.solution.backend.model.Transaction;
import com.mycompany.solution.backend.repository.TransactionRepository;

import java.time.LocalDate;
import java.util.List;

public class TransactionsService {

    private final TransactionRepository repository = new TransactionRepository();

    public List<Transaction> getTransactionsForLastDays(int numberOfDays) {
        return repository.getTransactions(LocalDate.now().minusDays(numberOfDays), LocalDate.now());
    }
}
