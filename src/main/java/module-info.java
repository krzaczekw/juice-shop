module com.mycompany.solution {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;
    requires itextpdf;
    requires java.sql;

    opens com.mycompany.solution to javafx.fxml;
    exports com.mycompany.solution.backend.model;
    opens com.mycompany.solution.backend.model to javafx.fxml;
    exports com.mycompany.solution.backend.repository;
    opens com.mycompany.solution.backend.repository to javafx.fxml;
    exports com.mycompany.solution.backend.service;
    opens com.mycompany.solution.backend.service to javafx.fxml;
    exports com.mycompany.solution;
}
